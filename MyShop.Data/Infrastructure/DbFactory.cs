﻿using System;

namespace MyShop.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        MyShopDbContext dbContext;
        public MyShopDbContext Init()
        {
            /*kiem tra dbContext
             *Neu null thi khoi tao
            */ 
            return dbContext ?? (dbContext = new MyShopDbContext());
        }
        protected override void DisposeCore()
        {
            if (dbContext != null )
            {
                dbContext.Dispose();
            }
        }
    }
}