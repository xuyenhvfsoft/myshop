﻿using System;

namespace MyShop.Data.Infrastructure
{
    //Giao tiep de khoi tao cac doi tuong Enity
    public interface IDbFactory : IDisposable
    {
        //Chi can mot phuong thuc de tao DbContext
        MyShopDbContext Init();
    }
}