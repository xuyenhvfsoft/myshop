﻿using System;

namespace MyShop.Data.Infrastructure
{
    public class Disposable : IDisposable
    {
        // Flag: Has Dispose already been called?
        private bool isDisposed;
        ~Disposable()
        {
            Dispose(false);
        }
        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this); //thu hoi bo nho
        }
        // Protected implementation of Dispose pattern.
        public void Dispose(bool disposing)
        {
            if (!isDisposed && disposing)
            {
                DisposeCore();
            }
            isDisposed = true;
        }
        // Protected implementation of Dispose pattern.
        protected virtual void DisposeCore()
        {
            
        }
    }
}